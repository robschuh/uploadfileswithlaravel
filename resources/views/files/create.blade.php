@extends('layouts.layout')

@section('content')

<div>
    <form method="post" id="frm" action="{{ route('upload') }}" files="true" enctype="multipart/form-data">
        {{ csrf_field() }}
        <label for="Archivo"><b>File: </b></label><input type="file" name="upload" required>
        <input type="submit" value="Upload file" >
    </form>
</div>
@endsection