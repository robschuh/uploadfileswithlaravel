@extends('layouts.layout')

@section('content')

<h1>File list</h1>

@if(!empty($files))
<div>
    <h1>Folder size: {{ $folder_size }} Kb </h1>
    @foreach ($files as $file )
    <h2>File details</h2>

    <ul class="">
        <li>Name: {{ $file->filename }}</li>
        <li>Type: {{ $file->mimeType }}</li>
        <li>Weight: {{ $file->size }} Kb</li>
 
        <li>File for <a href='/file/{{ $file->id }}'>download here</a></li>
        <li>Path file: storage/app/{{$file->filename}}</li>
        <li>
            <form action="/file/{{ $file->id }}" method='post'>  
                @method('DELETE')
                @csrf
                <button type='submit'>Delete file</button>
            </form>
        </li>
    </ul>    
    @endforeach
@else
<p>There are not any file yet, you can upload the <a href="{{ route('file.create') }}">first one here</a></p>
@endif
</div>
@endsection