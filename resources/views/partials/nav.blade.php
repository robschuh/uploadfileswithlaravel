<nav class="navbar" role="navigation" aria-label="main navigation">
  <div class="navbar-brand">
    <a class="navbar-item" href="http://homestead.test">
    
      <img src="http://masquebits.com/images/r0bertinski.jpg" width="112" height="28">

      
    </a>

    <a role="button" class="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
      <span aria-hidden="true"></span>
    </a>
  </div>

  <div id="navbarBasicExample" class="navbar-menu">
    <div class="navbar-start">
      <a class="navbar-item" href="/file" >
        Show files
      </a>

      <a class="navbar-item" href="/file/create" >
        New file
      </a>

     
    </div>

    <div class="navbar-end">
      <div class="navbar-item">
        <div class="buttons">
          <a href="/register"  class="button is-primary">
            <strong>Sign up</strong>
          </a>
          <a href="/login" class="button is-light">
            Log in
          </a>
        </div>
      </div>
    </div>
  </div>
</nav>