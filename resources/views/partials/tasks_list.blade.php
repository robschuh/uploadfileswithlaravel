<!-- if the project has tasks -->
@if($project->tasks->count())
    @foreach($project->tasks as $task)
        <form method="POST" action="/file/{{ $task->id }}">
            @method('PATCH')
            @csrf
            <div class="form-check">
                
                <input class="form-check-input" type="checkbox" name="completed" onChange="this.form.submit()" {{ $task->completed ? 'checked' : '' }}> 

                <label class="form-check-label {{ $task->completed ? 'is-completed' : '' }}" for="completed" >{{ $task->title}}</label>
                <span>Type: {{ $task->type }}</span>
            </div>
        </form>
    @endforeach
@else
    <p>There are not any file at the moment, please <a href="/file/create" class="btn btn-primary btn-lg active" role="button" aria-pressed="true">create the firts one here</a>  
@endif 