<?php // Code within app\Helpers\Helper.php

namespace App\Helpers;
use Illuminate\Support\Facades\Storage;

class Helper
{

     /**
     * Returns the folder size.
     *
     * @return boolean.
     */
    public static function getTotalFolderSize($folder = 'local')
    {
        $files_with_size = 0;
        $files = Storage::disk($folder)->files();
        foreach ($files as $key => $file) {
          $files_with_size += Storage::disk('local')->size($file);
        }

        return $files_with_size;
    }

     /**
     * Upload a file to the local folder.
     *
     * @return boolean.
     */

    public static function uploadFile($upload, $randFileName, $fileExt){
        
        return Storage::put($randFileName . '.' .  $fileExt, file_get_contents($upload));

    }
}