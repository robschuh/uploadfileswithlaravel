<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use \App\File;

class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //$files =  Storage::disk('public')->allFiles();
      /*  $folder_size = 0;
      //  dd(public_path());
        foreach( \File::allFiles(public_path('storage')) as $file)
        {
            $folder_size += $file->getSize();
        }

        $folder_size = number_format($folder_size / 1048576,2);
      */
        $files_with_size = 0;
        $files = Storage::disk('local')->files();
        foreach ($files as $key => $file) {
          //$files_with_size[$key]['name'] = $file;
          $files_with_size += Storage::disk('local')->size($file);
        }
      //  dd( $files );
        return view('files.index', [
            'files' =>  File::all(),
            'folder_size' => $files_with_size
        ]);
    }

  

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('files.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


       // Max 1024kb for uploaded file.
       request()->validate([
            'upload' => 'required|file|max:1024',
        ]);
        

        $randFileName = strtoupper(md5(uniqid(rand(),true))); 


        $fileExt  = $request->file('upload')->getClientOriginalExtension();

        $filePath = Storage::put($randFileName . '.' .  $fileExt, file_get_contents($request->file('upload')));

        if(!$filePath){
            return redirect()->route('file.index')->with('error','File was not uploaded');
        }

        File::create([
            'fileName' => $randFileName . '.' . $fileExt ,
            'mimeType' => $request->file('upload')->getClientMimeType(),
            'size'     => $request->file('upload')->getClientSize()
        ]);
       
        return redirect()->route('file.index')->with('success','Image uploaded successfully');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function show(File $file){
        return Storage::download($file->filename);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
       // dd($file->filename);
        if(Storage::delete([$file->filename])){
            $file->delete();
            return redirect()->route('file.index')->with('success','The file was deleted successfully');

        }

        return redirect()->route('file.index')->with('success','The file was not deleted');

    }
}
