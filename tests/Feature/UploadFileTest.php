<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Http\UploadedFile;
use App\Helpers\Helper;

class UploadFileTest extends TestCase
{

    public function testAvatarUpload()
    {

        // Create a fake file.
        $file = UploadedFile::fake()->create('document.txt', 1024);

        // Rand file name.
        $randFileName = strtoupper(md5(uniqid(rand(),true))); 

        // File ext
        $fileExt  =  $file->getClientOriginalExtension();

        // Get rensponse, true or false
        $response = Helper::uploadFile($file, $randFileName, $fileExt);

        // Do the assert.
        $this->assertTrue($response);

    }
}
