<?php
use App\Http\Controllers\FileController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::resource('/file', 'FileController');

Route::post('/upload','FileController@store')->name('upload');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
